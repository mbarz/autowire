import { GreetLib } from './greeter/greetlib';
import { Greeter } from './greeter';

import { inject } from './autowire';

const lib: GreetLib = new GreetLib();
lib.add('Hallo');

inject({
    GreetLib: lib
});

new Greeter().greet('Max');