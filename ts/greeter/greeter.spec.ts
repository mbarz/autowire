import { GreetLib } from './greetlib';
import { assert } from 'chai';
import {Greeter} from './greeter';

import {inject} from '../autowire';

describe('example tests', () => {

    const lib = new GreetLib;

    beforeEach(() => {
        
        
        inject({
            GreetLib: lib
        });
    })
    
    it('named greet', () => {
        lib.add('Hallo');
        let g = new Greeter().greet('Max');
        assert.equal(g, 'Hallo Max!');
    });

    it('anominous greet', () => {
        lib.add('¡Hola %s!');
        let g = new Greeter().greet();
        assert.equal(g, '¡Hola anominous user!');
    });

    it('anominous greet', () => {
        lib.add('Hallo');
        let g = new Greeter().greet();
        assert.equal(g, 'Hallo anominous user!');
    });
});