
export class GreetLib {

    greetings: string[] = [];

    constructor() {}

    getGreetings(): string[] {
        return [...this.greetings];
    }

    add(...greets) {
        for (const g of greets) {
            this.addGreeting(g);
        }
    }

    addGreeting(greeting: string) {
        this.greetings.push(greeting);
    }

    getGreeting(): string {
        const index = Math.round(Math.random() * (this.greetings.length-1));
        return this.greetings[index];
    }
}