import { GreetLib } from './greetlib';
import { inject, Autowired } from '../autowire';

declare type PropertyDecorator = (target: Object, propertyKey: string | symbol) => void;

type MyType = string;

class Greeter {

    @Autowired('GreetLib')
    private greetLib: GreetLib;

    constructor() { }

    public greet(user?: string) {
        if (!user) user = 'anominous user';
        const greet = this.greetLib.getGreeting();
        
        let message = `${greet} ${user}!`
        const hasPlaceHolder = greet.match(/%s/);
        if (hasPlaceHolder) {
            message = greet.replace(/%s/, user);
        }
        console.log(message);
        return message;
    }
}

export { Greeter };