type AutowireContext = { [key: string]: any }

export class AutowireService {
    context: AutowireContext
    getValue(type: string) {
        if (!this.context) throw new Error(`there is nothing for "${type}" in autowire context`);
        const value = this.context[type];
        if (!value) throw new Error(`missing injectable value for "${type}"`)
        return value;
    }
}
export const autowireService = new AutowireService();

export function inject(context: AutowireContext) {
    // console.log(`setting autwire context`);
    autowireService.context = context;
    return (target) => { };
}

function getName(t: any): string {
    var funcNameRegex = /function (.{1,})\(/;
    var results = (funcNameRegex).exec((t).constructor.toString());
    return (results && results.length > 1) ? results[1] : '';
};

export function Autowired(type: string) {

    return (target: any, key: string) => {

        let val;

        if (delete target[key]) {
            Object.defineProperty(target, key, {
                get: () => {
                    if (val) return val;
                    return autowireService.getValue(type)
                },
                set: (value) => {
                    const tName: string = getName(target);
                    console.warn(`overwriting autowired value for ${key} in ${tName}`)
                    val = value;
                }
            });
        }
    }
}